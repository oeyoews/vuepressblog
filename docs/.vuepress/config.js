const { searchPlugin } = require('@vuepress/plugin-search')
const { nprogressPlugin } = require('@vuepress/plugin-nprogress')

const { pwaPlugin } = require('@vuepress/plugin-pwa')
const { pwaPopupPlugin } = require('@vuepress/plugin-pwa-popup')

module.exports = {

  //base: "/vuepressBlogDemo",
  basse: "vues.vercel.app",

  // site
  lang: 'en-US',
  title: "🌹 Learning",
  description: " Just Do It!",

  themeConfig: { logo: 'https://cdn.jsDelivr.net/gh/oeyoews/img/oeyoew.jpeg', },
  //plugins: [ [ '@vuepress/plugin-search', { locales: { '/': { placeholder: 'Search', }, }, }, ], ],

  head: [
    ['link', {rel: 'manifest', href: '/manifest.json'}],
    ['meta', {name: 'theme-color', content: '#3eaf7c'},],
    ['link', {rel: 'shortcut icon', type: 'img/png', href: 'favicon.png'}],
    ["link", {rel: "stylesheet", type: "text/css", href: "https://fonts.googleapis.com/css2?family=Fira+Code&display=swap"},],
    ["link", {rel: "stylesheet", type: "text/css", href: "https://fonts.googleapis.com/css2?family=JetBrains+Mono&display=swap"},],
    ["link", {rel: "stylesheet", type: "text/css", href: "https://cdn.jsdelivr.net/gh/FortAwesome/Font-Awesome@6.x/css/all.css"},],
  ],
  lastUpdated: 'Last Updated',
  smoothcroll: true,
  plugins: [
    //nprogressPlugin(),
    searchPlugin({ }),
    pwaPlugin(
      { skipWaiting: true,}
    ),
    pwaPopupPlugin({
      locales: {
        '/': {
          message: 'New content is available.',
          buttonText: 'Refresh',
        },
        '/zh/': {
          message: '发现新内容可用',
          buttonText: '刷新',
        },
      }
      }),
  ],
}
